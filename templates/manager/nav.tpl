<div class="mb-4">
    <a class="btn btn-light m-1" href="{service controller=manager action=index}">{icon name=list} {tr}Instances{/tr}</a>
    <a class="btn btn-light m-1" href="{service controller=manager action=create}">{icon name=create} {tr}New Instance{/tr}</a>
    <a class="btn btn-light m-1" href="{service controller=manager action=virtualmin_create}">{icon name=create} {tr}New Virtualmin Instance{/tr}</a>
    <a class="btn btn-light m-1" href="{service controller=manager action=info}">{icon name=info} {tr}Info{/tr}</a>
    <a class="btn btn-light m-1" href="{service controller=manager action=requirements}">{icon name=check} {tr}Requirements{/tr}</a>
    <a class="btn btn-light m-1" href="{service controller=manager action=tiki_versions}">{icon name=list} {tr}Tiki Versions{/tr}</a>
    <a class="btn btn-light m-1" href="{service controller=manager action=test_send_email}">{icon name=envelope} {tr}Test Send Email{/tr}</a>
    <a class="btn btn-light m-1" href="{service controller=manager action=setup_watch}">{icon name="clock-o"} {tr}Setup Watch{/tr}</a>
</div>